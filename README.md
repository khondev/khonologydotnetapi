# Khonology Technical Assessment: Dotnet Web API #

Clone this repo onto your local computer and create a branch from the *master*. Please call your branch *assessment\YourName*.

### What is this repository for? ###

* This repo is for testing your key DotNet Skills to create a Web API suitable for consumption
* Version 2021.05.AF1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
* Install an IDE. We use [VS Code](https://code.visualstudio.com/) for .NET Core Development
* Clone the repo to your computer and create your own branch
* Install [DotNet Core](https://dotnet.microsoft.com/download)
* Run the application by typing in *dotnet run*
* The SQL Database that can be used is:
  * Server: **khondev.dedicated.co.za**
  * Port: **1433**
  * Username: **evaluation**
  * Password: **QdQCFAQr**
  * Database: **Northwind**

### Who do I talk to? ###

* For any questions during the assessment, please direct them to [developers@khonology.com](mailto:bernard.willer@khonology.com) where one of our developers will assist.

### Key Deliverables for the assessment:
* Create a controller to consume the *Orders* table data
* Create a view model as the response JSON object
  * Replace all foreign keys with the relevant description in their corresponding tables
* Create a controller method to insert a new record into the *Categories* table
* Create a controller method to update a record in the *Categories* table
* Create a controller method to delete a record in the *Categories* table